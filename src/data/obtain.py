import pandas as pd
import numpy as np


def read_train_data(path):
    return pd.read_csv(path,
                       na_values = [' ','','#NA','NA','NULL','NaN', 'nan', 'n/a'],
                      dtype = {'TotalCharges':np.float32,
                               'MonthlyCharges': np.float32}
    )