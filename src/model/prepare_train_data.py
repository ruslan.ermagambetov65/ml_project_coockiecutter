def prepare_data_for_train(df):
    df.drop(columns=['customerID', 'PaperlessBilling', 'PaymentMethod']
                 , axis=1, inplace=True)

    if 'Churn' in df.columns.tolist():
        X = df.drop(['Churn'], 1)
        y = (df['Churn'].values == 'Yes').astype(int)
    else:
        y = None

    return X, y


