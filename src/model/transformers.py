from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.preprocessing import LabelEncoder
from statsmodels.stats.outliers_influence import variance_inflation_factor
import numpy as np


class VIF_Columns_Dropper(TransformerMixin, BaseEstimator):
    """
    select most informative columns based on VIF
    """
    def __init__(self, thresh=.5):
        self.thresh = thresh

    def fit(self, X, y=None, **fit_params):
        return self

    def transform(self, X, y=None):
        X = X.copy().astype('float64')
        variables = list(range(X.shape[1]))
        dropped = True
        while dropped:
            dropped = False
            vif = [variance_inflation_factor(X[:, variables], i) for i in range(X[:, variables].shape[1])]
            maxloc = vif.index(max(vif))

            if max(vif) > self.thresh:
                del variables[maxloc]
                dropped = True

        return X[:, variables]

    def fit_transform(self, X, y=None, **fit_params):
        return self.transform(X, y)


class CustomLabelEncoder(TransformerMixin, BaseEstimator):
    """
    LabelEncoder() only on columns with less than nunique unique values
    """
    def __init__(self, nunique = 5):
        self.nunique = nunique

    def fit(self, X, y=None, **fit_params):
        return self

    def transform(self, X, y=None):
        X = X.copy()
        for i in range(X.shape[1]):
            # for encoding of all columns having unique values lower than 5
            if len(np.unique(X[:, i])) < 5:
                X[:, i] = LabelEncoder().fit_transform(X[:, i])
        return X

    def fit_transform(self, X, y=None, **fit_params):
        return self.transform(X,y)