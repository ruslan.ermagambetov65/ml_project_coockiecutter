import joblib
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC

from src.data.obtain import read_train_data
from src.model.prepare_train_data import prepare_data_for_train
from src.model.pipelines import make_transform_pipeline, tune_hyperparams, evaluate_on_test
from sklearn.naive_bayes import GaussianNB
from sklearn.linear_model import LogisticRegression


def train():
    """
    train churn model and save it
    @return: saves model to /models/
    """
    df = read_train_data('./data/raw/WA_Fn-UseC_-Telco-Customer-Churn.csv')
    X, y = prepare_data_for_train(df)

    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2 , random_state=32)

    MODEL_DICT = {
        GaussianNB:  None,
        LogisticRegression: None,
        KNeighborsClassifier: {
            'cls__n_neighbors': [5,7,9],
           'cls__weights': ['uniform','distance'],
           'cls__algorithm':['ball_tree', 'kd_tree', 'brute'],
            'cls__leaf_size':[15,30, 45],
            'cls__p':[1,2]
        },
        SVC: {'cls__C': [1, 10, 100, 1000],
                     'cls__kernel': ['rbf'],
                     'cls__gamma': [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]}
    }

    ESTIMATOR_LIST = []
    SCORES_LIST = []

    for model, params in MODEL_DICT.items():
        estimator = make_transform_pipeline(model())

        if params is not None:
            estimator, _ = tune_hyperparams(estimator, params, X_train, y_train)

        estimator.fit(X_train, y_train)
        test_metric = evaluate_on_test(estimator, X_test, y_test)
        print(test_metric)

        ESTIMATOR_LIST.append(estimator)
        SCORES_LIST.append(test_metric)

    max_score_idx = SCORES_LIST.index(max(SCORES_LIST))
    #save best model
    joblib.dump(ESTIMATOR_LIST[max_score_idx], filename='./models/model.bin')

if __name__ == '__main__':
    train()
