from sklearn.metrics import accuracy_score, precision_score, recall_score
from sklearn.model_selection import GridSearchCV
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import MinMaxScaler
from sklearn.impute import SimpleImputer
from sklearn.compose import make_column_selector as selector
from sklearn.compose import ColumnTransformer
import numpy as np

from src.model.transformers import VIF_Columns_Dropper, CustomLabelEncoder
from sklearn.preprocessing import LabelEncoder


def make_transform_pipeline(model):
    """ It creates the common pipeline for all models

    :param model:
        the final model for prediction, that support fit/predict/predict_proba
    :return:
        constructed pipeline, type of sklearn.pipeline.Pipeline
    """
    numerical_transformer = Pipeline(
        steps=[
            ('imputer', SimpleImputer()),
            ('scaler', MinMaxScaler())
        ]
    )

    categorical_transformer = Pipeline(
        steps=[
            ('imputer', SimpleImputer(strategy='most_frequent')),
            ('encoder', CustomLabelEncoder(nunique=5))
        ]
    )
    preprocessor = ColumnTransformer(transformers=[
        ('num', numerical_transformer, selector(dtype_include=np.number)),
        ('cat', categorical_transformer, selector(dtype_include="object"))
    ])

    cls = Pipeline(steps=[
        ('preprocessor', preprocessor),
        # ('dropper', VIF_Columns_Dropper(thresh=5.0)),
        ('cls', model)])

    return cls


def evaluate_on_test(classifier, X, y):
    """

    @param classifier: fitted cls
    @param X: data on which to predict
    @param y: label
    @return: recall score for predicitons vs labels
    """
    y_pred = classifier.predict(X)
    return recall_score(y, y_pred)


def tune_hyperparams(model, params_dict, X_train, y_train):
    """
    hyperparameter tuning
    @param model: model to tune
    @param params_dict: params grid to search
    @param X_train: train_data
    @param y_train: labels
    @return: model with found parameters
    """
    grid_search = GridSearchCV(estimator=model,
                               param_grid=params_dict,
                               scoring='accuracy',
                               cv=5,
                               n_jobs=-1)

    grid_search.fit(X_train, y_train)

    return grid_search.best_estimator_, grid_search.best_score_
