import numpy as np
from statsmodels.stats.outliers_influence import variance_inflation_factor

def calculate_vif_(X, thresh=5.0):
    variables = list(range(X.shape[1]))
    dropped = True
    while dropped:
        dropped = False
        vif = [variance_inflation_factor(X[:, variables], i) for i in range(X[:, variables].shape[1])]
        maxloc = vif.index(max(vif))
        print(max(vif))

        if max(vif) > thresh:
            del variables[maxloc]
            dropped = True

    print('Remaining variables:')
    print(variables)
    return X[:, variables]


def label_encoding_with_unique(X, label_encoder, n_unique):
    for i in range(X.shape[1]):
        # for encoding of all columns having unique values lower than 5
        if len(np.unique(X[:, i])) < 5:
            X[:, i] = label_encoder.fit_transform(X[:, i])
    return X

