import streamlit as st
import pandas as pd
from pandas.api.types import is_string_dtype
import joblib
import sys
sys.path.append('../../')
from src.data.obtain import read_train_data


# считываем данные
df = read_train_data('../../data/raw/WA_Fn-UseC_-Telco-Customer-Churn.csv')
df_cols = df.columns.tolist()
# создаем user inputs
options_list = []
for c in df_cols:
    nunique = df[c].nunique()
    if nunique == 2 or is_string_dtype(df[c]):
        option = st.sidebar.selectbox(
            f'Choose {c}',
            df[c].unique().tolist()
        )
        options_list.append(option)
    else:
        option = st.sidebar.slider(
            f'Choose {c}',
            int(df[c].min()),
            int(df[c].max()),
            step=1
        )
        options_list.append(option)

st.title('Churn Prediction Model')
# модель
MODEL = joblib.load('../../models/model.bin')
# прогнозы
X = pd.DataFrame.from_dict({k:[v] for k,v in zip(df_cols, options_list)})
# предсказанная вероятность
try:
    proba = MODEL.predict_proba(X[df_cols])[0][1]
    proba = str(round(proba * 100, 2))
except:
    proba = None
'Клиент со следующими характеристиками:'
X
'По модели:'
MODEL
''
f'С вероятностью **{proba}%**, откажется от наших услуг'