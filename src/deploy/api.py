import traceback
import joblib
import pandas as pd

from src.config import PORT
from src.model.prepare_train_data import prepare_data_for_train

from flask import Flask, request, jsonify

APP = Flask(__name__)
MODEL = None

def get_df(json_):
    df = pd.DataFrame(json_)
    return df

@APP.route('/predict', methods=['POST'])
def predict():
    try:
        json_ = request.get_json(force=True)
        APP.logger.info(f'processing request: {request}')
        df = get_df(json_)
        X, y = prepare_data_for_train(df)
        prediction = list(MODEL.predict(X))

        return jsonify({
            'prediction':str(prediction)
        })
    except:
        APP.logger.warn(f'traceback: {traceback.format_exc()}, while processing request: {request}')
        return jsonify({
            "trace": traceback.format_exc()
        })


def serve(debug=True):
    global MODEL
    MODEL = joblib.load('./models/model.bin')
    APP.logger.info(f'serving port: {PORT}')
    APP.run(port=PORT, debug=debug)
