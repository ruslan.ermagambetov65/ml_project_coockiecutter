import click

from src.deploy.api import serve
from src.model.train import train
import logging

ACTIONS = {
    'train': train,
    'serve': serve
}

logger = logging.getLogger(__name__)

@click.command()
@click.option('--action', default='.', help='options are: train | serve')
def main(action):
    """
    main function: train or serve churn model
    @param action: serve or train
    @return:
    """
    logger.info(f'processing action: {action}')
    ACTIONS[action]()


if __name__ == '__main__':
    logging.basicConfig(
        level=logging.DEBUG,
        format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
    )
    main()