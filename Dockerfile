FROM python:3

WORKDIR /opt/ml/app

COPY . .

RUN apt-get update && apt-get -y upgrade

RUN apt-get -y install libopenblas-dev liblapack-dev
RUN pip install --no-cache-dir -r requirements.txt

RUN python main.py --action train

EXPOSE 8501

# CMD [ "python", "./main.py", "--action", "serve" ]
WORKDIR ./src/deploy/

CMD [ "streamlit", "run", "./streamlit_app.py"]