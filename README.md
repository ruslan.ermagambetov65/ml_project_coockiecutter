# Churn Prediction + web-app via Streamlit
Обучает несколько моделей, выбирает из них лучшую, которая хостится в интерактивном веб-приложении
### Usage
`docker build -t churn_task_streamlit .`

`docker run -p 8501:8501 churn_task_streamlit:latest`

Затем идем на:

`localhost:8501/ (на винде - 192.168.99.100:8501/)`

